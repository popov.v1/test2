const ngrok = require('ngrok');
(async function() {
    console.log(await ngrok.connect({authtoken: process.env.NGROK_TOKEN, port: 3000}));
})();