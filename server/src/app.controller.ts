import { Controller, Get } from '@nestjs/common';

@Controller('')
export class AppController {
  constructor() {}

  @Get('/')
  root(): string {
    return 'Available only /api/getData, /api/getPipelines, /api/syncAll';
  }
}
