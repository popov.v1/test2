import {
  Entity,
  Column,
  PrimaryColumn,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
} from 'typeorm';
import { Lead } from './lead.entity';

@Entity()
export class Contact {
  @PrimaryColumn()
  id: number;

  @Column('varchar', { length: 1000, nullable: true })
  name: string;

  @Column('varchar', { length: 1000, nullable: true })
  first_name: string;

  @Column('varchar', { length: 1000, nullable: true })
  last_name: string;

  @Column({ type: 'integer', nullable: true })
  responsible_user_id: number;

  @Column({ type: 'integer', nullable: true })
  group_id: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  created_by: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  updated_by: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  created_at: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  updated_at: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  closed_at: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  closest_task_at: number;

  @Column('json', { nullable: true })
  custom_fields_values: boolean;

  @Column({ type: 'integer', nullable: true })
  account_id: number;

  @Column({ type: 'integer', nullable: false, default: -1 })
  amo_id: number;
}
