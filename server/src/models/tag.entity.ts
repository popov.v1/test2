import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class Tag {
  @PrimaryColumn()
  id: number;

  @Column('varchar', { length: 1000, nullable: true })
  name: string;

  @Column('varchar', { length: 1000, nullable: true })
  color: string;

  @Column({ type: 'integer', nullable: false, default: -1 })
  amo_id: number;
}
