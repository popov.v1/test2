import {
  Entity,
  Column,
  PrimaryColumn,
  JoinColumn,
  OneToOne,
  ManyToOne,
} from 'typeorm';
import { Pipeline } from './pipeline.entity';

@Entity()
export class Status {
  @PrimaryColumn()
  id: number;

  @Column('varchar', { length: 1000, nullable: true })
  name: string;

  @Column({ type: 'integer', nullable: true })
  sort: number;

  @Column({ type: 'boolean', nullable: true })
  is_editable: boolean;

  @Column('varchar', { length: 1000, nullable: true })
  color: string;

  @Column({ type: 'integer', nullable: true })
  type: number;

  @Column({ type: 'integer', nullable: true })
  account_id: number;

  @ManyToOne(() => Pipeline, { cascade: true })
  @JoinColumn({ name: 'pipeline_id' })
  pipeline: Pipeline;

  @Column({ type: 'integer', nullable: false, default: -1 })
  amo_id: number;
}
