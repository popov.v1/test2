import {
  Entity,
  Column,
  PrimaryColumn,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Contact } from './contact.entity';
import { Pipeline } from './pipeline.entity';
import { Tag } from './tag.entity';
import {Status} from "./status.entity";
import {User} from "./user.entity";

@Entity()
export class Lead {
  @PrimaryColumn()
  id: number;

  @Column('varchar', { length: 1000, nullable: true })
  name: string;

  @Column({ type: 'bigint', nullable: true })
  price: number;

  @Column({ type: 'integer', nullable: true })
  group_id: number;

  @Column('integer', { nullable: true })
  loss_reason_id: number;

  @Column('integer', { nullable: true })
  source_id: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  created_by: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  updated_by: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  created_at: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  updated_at: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  closed_at: number;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  closest_task_at: number;

  @Column('boolean', { nullable: true })
  is_deleted: boolean;

  @Column('json', { nullable: true })
  custom_fields_values: boolean;

  @Column('integer', { nullable: true })
  score: boolean;

  @Column({ type: 'integer', nullable: true })
  account_id: number;

  @ManyToOne(() => Pipeline, { cascade: true })
  @JoinColumn({ name: 'pipeline_id' })
  pipeline: Pipeline;

  @ManyToOne(() => Status, { cascade: true })
  @JoinColumn({ name: 'status_id' })
  status: Status;

  @ManyToMany(() => Tag, { cascade: true })
  @JoinTable()
  tags: Tag[];

  @ManyToMany(() => Contact, { cascade: true })
  @JoinTable()
  contacts: Contact[];

  @ManyToOne(() => User, { cascade: true })
  @JoinColumn({ name: 'responsible_user_id' })
  responsible_user: User;

  @Column({ type: 'integer', nullable: false, default: -1 })
  amo_id: number;

  @Column({ name: 'pipeline_id' })
  pipeline_id: number;

  @Column({ name: 'status_id' })
  status_id: number;

  @Column({ name: 'responsible_user_id' })
  responsible_user_id: number;

  _embedded: {
    contacts: Contact[];
    tags: Tag[];
  };
}
