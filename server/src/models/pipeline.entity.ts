import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Status } from './status.entity';
import { Lead } from './lead.entity';
import { Tag } from './tag.entity';

@Entity()
export class Pipeline {
  @PrimaryColumn()
  id: number;

  @Column('varchar', { length: 1000, nullable: true })
  name: string;

  @Column({ type: 'integer', nullable: true })
  sort: number;

  @Column({ type: 'boolean', nullable: true })
  is_main: boolean;

  @Column({ type: 'boolean', nullable: true })
  is_unsorted_on: boolean;

  @Column({ type: 'boolean', nullable: true })
  is_archive: boolean;

  @Column({ type: 'integer', nullable: true })
  account_id: number;

  @OneToMany(() => Lead, (lead) => lead.pipeline)
  leads: Lead[];

  @OneToMany(() => Status, (status) => status.pipeline)
  statuses: Status[];

  _embedded: {
    statuses: Status[];
  };

  @Column({ type: 'integer', nullable: false, default: -1 })
  amo_id: number;
}
