import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryColumn()
  id: number;

  @Column('varchar', { length: 1000, nullable: true })
  name: string;

  @Column('varchar', { length: 1000, nullable: true })
  email: string;

  @Column('varchar', { length: 6, nullable: true })
  lang: string;

  @Column('json', { nullable: true })
  rights: string;

  @Column({ type: 'integer', nullable: false, default: -1 })
  amo_id: number;

  _embedded: {
    roles: any[];
    groups: any[];
  };
}
