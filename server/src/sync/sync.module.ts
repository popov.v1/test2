import { Module } from '@nestjs/common';
import { SyncService } from './sync.service';
import { AccountsModule } from 'src/accounts/accounts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lead } from '../models/lead.entity';
import { Contact } from 'src/models/contact.entity';
import { Pipeline } from '../models/pipeline.entity';
import { User } from '../models/user.entity';
import { Status } from '../models/status.entity';
import { Tag } from '../models/tag.entity';
import { Account } from "../accounts/account.entity";

@Module({
  imports: [
    AccountsModule,
    TypeOrmModule.forFeature([Account, Lead, Contact, Pipeline, User, Status, Tag]),
  ],
  providers: [SyncService],
  exports: [SyncService],
})
export class SyncModule {}
