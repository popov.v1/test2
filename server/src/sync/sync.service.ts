import { Injectable } from '@nestjs/common';
import { AccountsService } from 'src/accounts/accounts.service';
import { Repository } from 'typeorm';
import { IResponse } from '../interfaces/IResponse';
import { IId } from '../interfaces/IId';
import { Lead } from '../models/lead.entity';
import { Contact } from '../models/contact.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Pipeline } from '../models/pipeline.entity';
import { User } from '../models/user.entity';
import { Status } from '../models/status.entity';
import { Tag } from '../models/tag.entity';
import { Account } from "../accounts/account.entity";
import {IMessage} from "../interfaces/IMessage";

@Injectable()
export class SyncService {
  constructor(
    private accountsService: AccountsService,
    @InjectRepository(Account)
    private accountsRepo: Repository<Account>,
    @InjectRepository(Lead)
    private leadRepo: Repository<Lead>,
    @InjectRepository(Contact)
    private contactRepo: Repository<Contact>,
    @InjectRepository(Pipeline)
    private pipelineRepo: Repository<Pipeline>,
    @InjectRepository(User)
    private userRepo: Repository<User>,
    @InjectRepository(Status)
    private statusRepo: Repository<Status>,
    @InjectRepository(Tag)
    private tagsRepo: Repository<Tag>,
  ) {}

  private async createOrUpdate<TModel extends IId>(
    model: new () => TModel,
    repository: Repository<TModel>,
    element: any,
    amoId: number,
  ): Promise<TModel> {
    let target: any = await repository.findOneBy({
      id: element.id,
    });
    if (!target) {
      target = new model();
    }
    for (const name of Object.keys(element) as Array<keyof TModel>) {
      target[name] = element[name];
    }
    target.amo_id = amoId;
    await repository.save(target);
    return target;
  }

  private async syncModel<TModel extends IId, TResponse extends IResponse>(
    model: new () => TModel,
    amoId: number,
    repository: Repository<TModel>,
    apiUri: string,
    params = '',
  ): Promise<TModel[]> {
    try {
      const api = this.accountsService.createConnector(amoId);
      const user = await this.accountsService.findByAmoId(amoId);
      let page = 1;
      const synced: TModel[] = [];
      while (true) {
        const { data } = await api.get<TResponse>(
          apiUri + `?filter[updated_at][from]=${user.last_sync_time}&filter[updated_at][to]=${(Math.floor(new Date().getTime()) / 1000) + 86400}&page=${page}&limit=50${params}`,
        );
        if (!data) return synced;
        const keys = Object.keys(data._embedded);
        const list: any[] = data._embedded[keys[0]];
        for (const element of list) {
          synced.push(
            await this.createOrUpdate(model, repository, element, amoId),
          );
        }
        if (!data._links || !data._links.next) return synced;
        page++;
      }
    } catch (e) {
      console.log(e);
    }
  }

  private async syncContacts(amoId: number): Promise<Contact[]> {
    try {
      return await this.syncModel(
        Contact,
        amoId,
        this.contactRepo,
        '/api/v4/contacts',
      );
    } catch (e) {
      console.log(e);
    }
  }

  private async syncLeads(amoId: number, contacts: Contact[], pipelines: Pipeline[], users: User[]): Promise<Lead[]> {
    try {
      const leads = await this.syncModel(
        Lead,
        amoId,
        this.leadRepo,
        '/api/v4/leads',
        '&with=contacts',
      );
      const statuses = await this.statusRepo.find({ where: { amo_id: amoId }});
      for (const lead of leads) {
        const dbContacts: Contact[] = [];
        const dbTags: Tag[] = [];
        for (const contact of lead._embedded.contacts) {
          dbContacts.push(contacts.find((x) => x.id === contact.id));
        }
        for (const tag of lead._embedded.tags) {
          dbTags.push(
            await this.createOrUpdate(Tag, this.tagsRepo, tag, amoId),
          );
        }
        lead.contacts = dbContacts;
        lead.tags = dbTags;
        lead.responsible_user = users.find((x) => x.id === lead.responsible_user_id);
        lead.pipeline = pipelines.find((x) => x.id === lead.pipeline_id);
        lead.status = statuses.find((x) => x.id === lead.status_id);
        await this.leadRepo.manager.save(lead);
      }
      return leads;
    } catch (e) {
      console.log(e);
    }
  }

  private async syncPipelines(amoId: number): Promise<Pipeline[]> {
    try {
      const pipelines = await this.syncModel(
        Pipeline,
        amoId,
        this.pipelineRepo,
        '/api/v4/leads/pipelines',
      );
      for (const pipeline of pipelines) {
        for (const status of pipeline._embedded.statuses) {
          const dbStatus = await this.createOrUpdate(
            Status,
            this.statusRepo,
            status,
            amoId,
          );
          dbStatus.pipeline = pipeline;
          await this.statusRepo.manager.save(dbStatus);
        }
      }
      return pipelines;
    } catch (e) {
      console.log(e);
    }
  }

  private async syncUsers(amoId: number): Promise<User[]> {
    try {
      return await this.syncModel(User, amoId, this.userRepo, '/api/v4/users');
    } catch (e) {
      console.log(e);
    }
  }

  public async syncAll(amoId: number): Promise<IMessage> {
    const contacts = await this.syncContacts(amoId);
    const pipelines = await this.syncPipelines(amoId);
    const users = await this.syncUsers(amoId);
    await this.syncLeads(amoId, contacts, pipelines, users);
    await this.accountsService.updateLastSyncTime(amoId, new Date().getTime() / 1000);
    return { message: 'OK', status: 200 };
  }
}
