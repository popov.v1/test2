import { Injectable } from '@nestjs/common';
import { AccountsService } from 'src/accounts/accounts.service';
import { Brackets, Repository } from 'typeorm';
import { Lead } from '../models/lead.entity';
import { Contact } from '../models/contact.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Pipeline } from '../models/pipeline.entity';
import { User } from '../models/user.entity';
import { Status } from '../models/status.entity';
import { IDataResponse } from '../interfaces/IDataResponse';

@Injectable()
export class ApiService {
  constructor(
    private accountsService: AccountsService,
    @InjectRepository(Lead)
    private leadRepo: Repository<Lead>,
    @InjectRepository(Contact)
    private contactRepo: Repository<Contact>,
    @InjectRepository(Pipeline)
    private pipelineRepo: Repository<Pipeline>,
    @InjectRepository(User)
    private userRepo: Repository<User>,
    @InjectRepository(Status)
    private statusRepo: Repository<Status>,
  ) {}

  private getStartDayTime(time: number): number {
    if(isNaN(+time)) return null;
    const date = new Date(time*1000);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    return Math.floor(date.getTime() / 1000);
  }

  public async getData(
    amoId: number,
    pipelineId: number,
    query: string,
  ): Promise<IDataResponse> {
    try {
      const leads = await this.leadRepo.createQueryBuilder('lead')
          .leftJoin('lead.contacts', 'contacts')
          .leftJoin('lead.tags', 'tags')
          .leftJoin('lead.status', 'status')
          .leftJoin('lead.responsible_user', 'responsible_user')
          .andWhere({ 'pipeline_id': pipelineId })
          .andWhere({ 'amo_id': amoId })
          .andWhere(
              new Brackets((qb) => {
                qb.orWhere("lead.name LIKE :name", {
                    name: `%${query}%`,
                }).orWhere("lead.price LIKE :price", {
                    price: `%${query}%`,
                }).orWhere("lead.created_at >= :created_at AND lead.created_at <= :created_at_to", {
                    created_at: `${this.getStartDayTime(Number(query))}`,
                    created_at_to: `${this.getStartDayTime(Number(query))+86400}`
                }).orWhere("contacts.name LIKE :name", {
                    name: `%${query}%`,
                }).orWhere("tags.name LIKE :name", {
                    name: `%${query}%`,
                }).orWhere("status.name LIKE :name", {
                    name: `%${query}%`,
                }).orWhere("responsible_user.name LIKE :name", {
                    name: `%${query}%`,
                });
              })
          )
          .leftJoinAndSelect('lead.contacts', 'contacts2')
          .leftJoinAndSelect('lead.tags', 'tags2')
          .getMany();
      const users = await this.userRepo.find({ where: { amo_id: amoId } });
      return { leads, users };
    } catch (e) {
      console.log(e);
    }
  }

  public async getPipelines(
      amoId: number,
  ): Promise<Pipeline[]> {
    try {
      return await this.pipelineRepo.find({
        relations: ['statuses'],
        where: { amo_id: amoId },
      });
    } catch (e) {
      console.log(e);
    }
  }
}
