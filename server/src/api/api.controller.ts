import { Controller, Get, Post, Query, Req } from '@nestjs/common';
import { Request } from 'src/types/request';
import { ApiService } from './api.service';
import { SyncService } from "../sync/sync.service";
import { IDataResponse } from "../interfaces/IDataResponse";
import { Pipeline } from "../models/pipeline.entity";
import { IMessage } from "../interfaces/IMessage";

@Controller('api')
export class ApiController {
  constructor(
      private apiService: ApiService,
      private syncService: SyncService
  ) {}

  @Get('/getData')
  public async getData(@Req() req: Request, @Query() query): Promise<IDataResponse> {
    return await this.apiService.getData(query.amoId, query.pipelineId, query.query);
  }

  @Get('/getPipelines')
  public async getPipelines(@Req() req: Request, @Query() query): Promise<Pipeline[]> {
    return await this.apiService.getPipelines(query.amoId);
  }

  @Post('/syncAll')
  public async syncAll(@Req() req: Request, @Query() query): Promise<IMessage> {
    return await this.syncService.syncAll(query.amoId);
  }
}
