import { Module } from '@nestjs/common';
import { ApiService } from './api.service';
import { ApiController } from './api.controller';
import { AccountsModule } from 'src/accounts/accounts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lead } from '../models/lead.entity';
import { Contact } from 'src/models/contact.entity';
import { Pipeline } from '../models/pipeline.entity';
import { User } from '../models/user.entity';
import { Status } from '../models/status.entity';
import { SyncModule } from "../sync/sync.module";

@Module({
  imports: [
    AccountsModule,
    SyncModule,
    TypeOrmModule.forFeature([Lead, Contact, Pipeline, User, Status]),
  ],
  providers: [ApiService],
  controllers: [ApiController],
})
export class ApiModule {}
