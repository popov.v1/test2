import { All, Controller, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import { IAuthCallbackQuery } from 'src/interfaces/IAuthCallbackQuery';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @All('/callback')
  async callback(@Query() query: IAuthCallbackQuery, @Res() res: Response) {
    const link = await this.authService.performCallback(query);
    return res.redirect(link);
  }
}
