import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { GrantTypes } from 'src/enums/grant-types.enum';
import { IAuthCallbackQuery } from 'src/interfaces/IAuthCallbackQuery';
import { ConfigService } from '@nestjs/config';
import { IOAuthField } from 'src/interfaces/IOAuthField';
import { AccountsService } from 'src/accounts/accounts.service';
import * as jwt from 'jsonwebtoken';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class AuthService {
  constructor(
    private configService: ConfigService,
    @Inject(forwardRef(() => AccountsService))
    private accountsService: AccountsService,
    private readonly httpService: HttpService,
  ) {}

  async performCallback(query: IAuthCallbackQuery): Promise<string> {
    const oauth: IOAuthField = await this.getNewTokens(
      query.code,
      query.referer,
    );
    const decoded = jwt.decode(oauth.accessToken, { json: true });
    const account = await this.accountsService.findByAmoId(decoded.account_id);
    if (!account) {
      await this.accountsService.create({
        amoId: decoded.account_id,
        domain: query.referer,
        oauth,
      });
    } else {
      await this.accountsService.update(account.id, {
        domain: query.referer,
        oauth,
      });
    }
    return `${this.configService.get('clientUri')}?amoId=${
      decoded.account_id || account.id
    }`;
  }

  async getNewTokens(
    i: string,
    domain: string,
    type: GrantTypes = GrantTypes.AuthCode,
  ) {
    try {
      const { data } = await this.httpService.axiosRef.post(
        `https://${domain}/oauth2/access_token`,
        {
          client_id: this.configService.get('clientId'),
          client_secret: this.configService.get('clientSecret'),
          redirect_uri: this.configService.get('redirectUri'),
          grant_type: type,
          [type === GrantTypes.AuthCode ? 'code' : 'refresh_token']: i,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );

      return {
        accessToken: data.access_token,
        refreshToken: data.refresh_token,
        expire: Number(new Date()) + data.expires_in * 1000,
      };
    } catch (err) {
      console.log({
        req: {
          url: err.response.config.url,
          data: err.response.config.data,
        },
        res: {
          data: err.response.data,
        },
      });
      throw new Error(err.response.data.detail);
    }
  }
}
