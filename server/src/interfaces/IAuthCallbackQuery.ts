export interface IAuthCallbackQuery {
  code: string;
  state: string;
  referer: string;
  client_id: string;
}
