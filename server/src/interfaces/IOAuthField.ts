export interface IOAuthField {
  accessToken: string;
  refreshToken: string;
  expire: number;
}
