export interface IResponse {
  _embedded: any;
  _links: {
    next: any;
  };
}
