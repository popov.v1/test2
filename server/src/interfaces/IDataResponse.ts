import { Lead } from '../models/lead.entity';
import { User } from '../models/user.entity';
import { IMessage } from './IMessage';

export interface IDataResponse {
  leads: Lead[] | IMessage;
  users: User[] | IMessage;
}
