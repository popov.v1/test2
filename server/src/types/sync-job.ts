import { Job } from 'bull';
import { Account } from 'src/accounts/account.entity';

export type SyncJob = Job<{
  account: Account;
}>;
