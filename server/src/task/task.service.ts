import { Injectable, OnModuleInit  } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Cron, CronExpression } from "@nestjs/schedule";
import { Account } from "../accounts/account.entity";
import { SyncService } from "../sync/sync.service";

@Injectable()
export class TaskService implements OnModuleInit {
  constructor(
    private syncService: SyncService,
    @InjectRepository(Account)
    private accountsRepo: Repository<Account>,
  ) {}

  onModuleInit() {
    this.handleCron();
  }

  @Cron(CronExpression.EVERY_MINUTE)
  public async handleCron() {
    const users = await this.accountsRepo.find();
    for (const user of users) {
      await this.syncService.syncAll(user.amoId);
      console.log('Synced for: ' + user.amoId);
    }
  }
}
