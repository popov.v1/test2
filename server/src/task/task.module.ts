import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from "../accounts/account.entity";
import {SyncModule} from "../sync/sync.module";

@Module({
  imports: [
    SyncModule,
    TypeOrmModule.forFeature([Account]),
  ],
  providers: [TaskService],
})
export class TaskModule {}
