import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { Account } from './account.entity';
import { AccountsService } from './accounts.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([Account]),
    HttpModule,
  ],
  providers: [AccountsService],
  exports: [AccountsService],
})
export class AccountsModule {}
