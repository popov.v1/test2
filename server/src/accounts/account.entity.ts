import { IOAuthField } from 'src/interfaces/IOAuthField';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amoId: number;

  @Column()
  domain: string;

  @CreateDateColumn({
    type: 'int',
    width: 11,
    nullable: true,
    default: () => '0',
  })
  last_sync_time: number;

  get url(): string {
    return `https://${this.domain}`;
  }

  @Column({ type: 'json' })
  oauth: IOAuthField;
}
