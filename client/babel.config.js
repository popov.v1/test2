module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    'lodash',
    [
      'import',
      {
        libraryName: 'ant-design-vue',
        libraryDirectory: 'es',
        style: true
      }
    ],
    [
      'import',
      {
        libraryName: '@ant-design-vue/icons-vue',
        libraryDirectory: 'es/icons',
        camel2DashComponentName: false
      },
      'ant-design-icons-vue'
    ]
  ]
}
