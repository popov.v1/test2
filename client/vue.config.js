require('dotenv').config();

const { NODE_ENV = 'development', WEBPACK_PORT } = process.env;
const publicPath = `http://localhost:${WEBPACK_PORT}`

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath,
  css: {
    extract: false,
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    }
  },
})
