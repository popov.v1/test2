import { IStatus } from './IStatus';

export interface IPipeline {
  id: number;
  name: string;
  statuses: IStatus[];
}
