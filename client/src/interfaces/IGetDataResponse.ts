import { ILead } from './ILead';
import { IUser } from './IUser';
import { IMessage } from './IMessage';

export interface IGetDataResponse {
  leads: ILead[] | IMessage;
  users: IUser[] | IMessage;
}
